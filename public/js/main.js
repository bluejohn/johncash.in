(function() {
  'use strict'

  const d = document,
      projects = d.querySelector('.projects'),
      articles = d.querySelector('.articles'),
      profile = d.querySelector('.profile')


  let profileView = false
  function showProfile() {
    let tl1 = new TimelineMax()
    if (!profileView) {
      profileView = true
      TweenMax.set([projects, articles], {y: -20})
      tl1
        .to([projects, articles], 0.8, {
          opacity: 0,
          y: 20,
          onComplete: function() {
            TweenMax.set([projects, articles], {display: 'none'})
            TweenMax.set(profile, {y: -20})
            TweenMax.to(profile, 0.8, {
              y: 20,
              display: 'block',
              opacity: 1,
            })
          }
      })
    } else {
      profileView = false
      tl1
        .to(profile, 0.8, {
          y: -20,
          opacity: 0,
          display: 'none',
          onComplete: function() {
            TweenMax.to([projects, articles], 0.8,{
              y: -20,
              opacity: 1,
              display: 'block'
            })
          }
        })
    }
  }

  let clickProfile = d.querySelector('.profile_btn'),
      closeProfile = d.querySelector('.close_profile'),
      activeProject = d.querySelector('.active_project'),
      tl2 = new TimelineMax(),
      projHeading = d.querySelector('.project_heading'),
      projDesc = d.querySelector('.project_description'),
      projImg = d.querySelector('.project_imgs'),
      projectView = false,
      cached

    clickProfile.addEventListener('click', showProfile, false)
    closeProfile.addEventListener('click', showProfile, false)

  const Projects = {

    p7: {
      title: 'Rugby 7s',
      description: 'Front-end development of main Rugby 7s website and development of shazam' +
      ' activation game. The game allowed users to flick the ball towards the goal posts. If the' +
      ' use hit one of three icons, they would win one of three prizes. Game was built using GSAP ' +
      'animation library.',
      img: [
        'img/runwild-desktop1.jpg',
        'img/runwild-desktop2.jpg',
        'img/runwild-mobile.png',
        'img/runwild-mobile-game1.png',
        'img/runwild-mobile-game2.png',
        'img/runwild-mobile-game3.png',
        'img/runwild-mobile-game4.png',
        'img/runwild-mobile-game5.png',
      ]
    },
    p6: {
      title: 'Alfa game',
      description: 'Game built with Phaser Javascript library for a shazam activation. Users' +
      ' need to match selections to win a prize.',
      img: [
        'img/alfa1-mobile1.jpg',
        'img/alfa1-mobile2.jpg',
        'img/alfa1-mobile3.jpg'
      ]
    },
    p5: {
      title: 'Landing pages & rich media creatives',
      description: 'Landing pages and rich media interstitials built with MRAID 2.0.',
      img: [
        'img/dv-lp-mobile1.jpg',
        'img/dv-lp-mobile2.jpg',
        'img/dv-lp-mobile3.jpg',
        'img/dv-rich-mobile1.jpg',
        'img/dv-rich-mobile2.jpg',
        'img/dv-rich-mobile3.jpg',
        'img/dv-rich-mobile4.jpg',
        'img/dv-rich-mobile5.jpg',
        'img/dv-rich-mobile6.jpg',
        'img/dv-rich-mobile7.jpg',
        'img/dv-rich-mobile8.jpg'
      ]
    },
    p4: {
      title: 'Digital Venture & in-house tools',
      description: 'Development of main Digital Venture Website and in-house tools, used by both ' +
      'the production team and sales to track campaign based tasks and journeys.',
      img: [
        'img/dv-desktop1.jpg',
        'img/dv-desktop2.jpg',
        'img/dv-desktop3.jpg',
        'img/dv-mobile1.png',
        'img/dv-mobile2.png',
        'img/dv-mobile3.png',
        'img/dv-tool1.jpg',
        'img/dv-tool2.jpg',
        'img/dv-tool3.jpg',
        'img/dv-tool4.jpg',
        'img/dv-tool5.jpg',
        'img/dv-tool6.jpg'
      ]
    },
    p3: {
      title: 'Get Ready Napier',
      description: 'Employed as an creative intern at Napier my key responsibilities were overseeing the redesign and development of the official Get Ready student portal. From working closely with facility staff and planning the initial brief and design to full development of site features such as animated info-grahics, user testing, responsive' +
      ' design and ﬁnal deployment. \n' +
      '\n',
      img: [
        'img/getready-desktop5.jpg',
        'img/getready-desktop3.jpg',
        'img/getready-desktop2.jpg',
        'img/getready1-mobile.png',
        'img/getready2-mobile.png'
      ]
    },
    p2: {
      title: 'scpianolessons.ie',
      description: 'Website designed/developed for local piano teacher.',
      img: [
        'img/scpianolessons-desktop1.jpg',
        'img/scpianolessons-desktop2.jpg',
        'img/scpianolessons-desktop3.jpg',
        'img/scpianolessons-mobile1.png',
        'img/scpianolessons-mobile2.png',
        'img/scpianolessons-mobile3.png'
      ]
    },
    p1: {
      title: 'clarkesbar.ie',
      description: 'Website designed/developed for Irish bar.',
      img: [
        'img/clarkes-desktop1.jpg',
        'img/clarkes-mobile1.png',
        'img/clarkes-mobile2.png',
      ]
    },
    showProject: function(i) {
      //console.log(this['p' + i].title)
      projHeading.innerText = this['p' + i].title
      projDesc.innerText = this['p' + i].description
      let projectImg = this['p' + i].img,
          mobStr = 'mobile',
          deskStr = 'desktop'

      if (cached !== i) {
        projImg.innerHTML = '';

        for (let i = 0; i < projectImg.length; i++) {
          let img = d.createElement('img')
          img.src = projectImg[i]

          if (projectImg[i].indexOf(mobStr) !== -1) {
            img.className = 'mobile_img'
          } else {
            img.className = 'desktop_img'
          }
          projImg.appendChild(img);
          cached = i
        }
      }

      if (!profileView) {
        projectView = true
        TweenMax.set([projects, articles], {y: -20})
        tl2
          .to([projects, articles], 0.8, {
            opacity: 0,
            y: 20,
            onComplete: function() {
              TweenMax.set([projects, articles], {display: 'none'})
              TweenMax.set(activeProject, {y: -20})
              TweenMax.to(activeProject, 0.8, {
                y: 20,
                display: 'block',
                opacity: 1,
              })
            }
          })
      }
    },
    closeProject: function() {
      projectView = false
      tl2
        .to(activeProject, 0.8, {
          y: -25,
          opacity: 0,
          display: 'none',
          onComplete: function() {
            TweenMax.to([projects, articles], 0.8,{
              y: -25,
              opacity: 1,
              display: 'block'
            })
          }
        })
    }
  }

  let p = Object.create(Projects),
      clickedProject = d.getElementsByClassName('project'),
      closeProject = d.querySelector('.close_project')

  closeProject.addEventListener('click', function(e) {
    p.closeProject()
  })

  for (let i = 0; i < clickedProject.length; i++) {
    clickedProject[i].addEventListener('click', function(e) {
      switch(this.getAttribute('data-id')) {
        case '1':
          p.showProject('7')
          break;
        case '2':
          p.showProject('6')
          break;
        case '3':
          p.showProject('5')
          break;
        case '4':
          p.showProject('4')
          break;
        case '5':
          p.showProject('3')
          break;
        case '6':
          p.showProject('2')
          break;
        case '7':
          p.showProject('1')
          break;
      }
    })
  }

  let clickedArticle = d.getElementsByClassName('article')
  for (let i = 0; i < clickedArticle.length; i++) {
    clickedArticle[i].addEventListener('click', function(e) {
      switch(this.getAttribute('data-id')) {
        case 'a1':
          window.open('https://johncash.in/staging/pokedex/index.html', '_blank');
          break;
      }
    })
  }

})()
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4uanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJtYWluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCkge1xuICAndXNlIHN0cmljdCdcblxuICBjb25zdCBkID0gZG9jdW1lbnQsXG4gICAgICBwcm9qZWN0cyA9IGQucXVlcnlTZWxlY3RvcignLnByb2plY3RzJyksXG4gICAgICBhcnRpY2xlcyA9IGQucXVlcnlTZWxlY3RvcignLmFydGljbGVzJyksXG4gICAgICBwcm9maWxlID0gZC5xdWVyeVNlbGVjdG9yKCcucHJvZmlsZScpXG5cblxuICBsZXQgcHJvZmlsZVZpZXcgPSBmYWxzZVxuICBmdW5jdGlvbiBzaG93UHJvZmlsZSgpIHtcbiAgICBsZXQgdGwxID0gbmV3IFRpbWVsaW5lTWF4KClcbiAgICBpZiAoIXByb2ZpbGVWaWV3KSB7XG4gICAgICBwcm9maWxlVmlldyA9IHRydWVcbiAgICAgIFR3ZWVuTWF4LnNldChbcHJvamVjdHMsIGFydGljbGVzXSwge3k6IC0yMH0pXG4gICAgICB0bDFcbiAgICAgICAgLnRvKFtwcm9qZWN0cywgYXJ0aWNsZXNdLCAwLjgsIHtcbiAgICAgICAgICBvcGFjaXR5OiAwLFxuICAgICAgICAgIHk6IDIwLFxuICAgICAgICAgIG9uQ29tcGxldGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgVHdlZW5NYXguc2V0KFtwcm9qZWN0cywgYXJ0aWNsZXNdLCB7ZGlzcGxheTogJ25vbmUnfSlcbiAgICAgICAgICAgIFR3ZWVuTWF4LnNldChwcm9maWxlLCB7eTogLTIwfSlcbiAgICAgICAgICAgIFR3ZWVuTWF4LnRvKHByb2ZpbGUsIDAuOCwge1xuICAgICAgICAgICAgICB5OiAyMCxcbiAgICAgICAgICAgICAgZGlzcGxheTogJ2Jsb2NrJyxcbiAgICAgICAgICAgICAgb3BhY2l0eTogMSxcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgfVxuICAgICAgfSlcbiAgICB9IGVsc2Uge1xuICAgICAgcHJvZmlsZVZpZXcgPSBmYWxzZVxuICAgICAgdGwxXG4gICAgICAgIC50byhwcm9maWxlLCAwLjgsIHtcbiAgICAgICAgICB5OiAtMjAsXG4gICAgICAgICAgb3BhY2l0eTogMCxcbiAgICAgICAgICBkaXNwbGF5OiAnbm9uZScsXG4gICAgICAgICAgb25Db21wbGV0ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBUd2Vlbk1heC50byhbcHJvamVjdHMsIGFydGljbGVzXSwgMC44LHtcbiAgICAgICAgICAgICAgeTogLTIwLFxuICAgICAgICAgICAgICBvcGFjaXR5OiAxLFxuICAgICAgICAgICAgICBkaXNwbGF5OiAnYmxvY2snXG4gICAgICAgICAgICB9KVxuICAgICAgICAgIH1cbiAgICAgICAgfSlcbiAgICB9XG4gIH1cblxuICBsZXQgY2xpY2tQcm9maWxlID0gZC5xdWVyeVNlbGVjdG9yKCcucHJvZmlsZV9idG4nKSxcbiAgICAgIGNsb3NlUHJvZmlsZSA9IGQucXVlcnlTZWxlY3RvcignLmNsb3NlX3Byb2ZpbGUnKSxcbiAgICAgIGFjdGl2ZVByb2plY3QgPSBkLnF1ZXJ5U2VsZWN0b3IoJy5hY3RpdmVfcHJvamVjdCcpLFxuICAgICAgdGwyID0gbmV3IFRpbWVsaW5lTWF4KCksXG4gICAgICBwcm9qSGVhZGluZyA9IGQucXVlcnlTZWxlY3RvcignLnByb2plY3RfaGVhZGluZycpLFxuICAgICAgcHJvakRlc2MgPSBkLnF1ZXJ5U2VsZWN0b3IoJy5wcm9qZWN0X2Rlc2NyaXB0aW9uJyksXG4gICAgICBwcm9qSW1nID0gZC5xdWVyeVNlbGVjdG9yKCcucHJvamVjdF9pbWdzJyksXG4gICAgICBwcm9qZWN0VmlldyA9IGZhbHNlLFxuICAgICAgY2FjaGVkXG5cbiAgICBjbGlja1Byb2ZpbGUuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBzaG93UHJvZmlsZSwgZmFsc2UpXG4gICAgY2xvc2VQcm9maWxlLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgc2hvd1Byb2ZpbGUsIGZhbHNlKVxuXG4gIGNvbnN0IFByb2plY3RzID0ge1xuXG4gICAgcDc6IHtcbiAgICAgIHRpdGxlOiAnUnVnYnkgN3MnLFxuICAgICAgZGVzY3JpcHRpb246ICdGcm9udC1lbmQgZGV2ZWxvcG1lbnQgb2YgbWFpbiBSdWdieSA3cyB3ZWJzaXRlIGFuZCBkZXZlbG9wbWVudCBvZiBzaGF6YW0nICtcbiAgICAgICcgYWN0aXZhdGlvbiBnYW1lLiBUaGUgZ2FtZSBhbGxvd2VkIHVzZXJzIHRvIGZsaWNrIHRoZSBiYWxsIHRvd2FyZHMgdGhlIGdvYWwgcG9zdHMuIElmIHRoZScgK1xuICAgICAgJyB1c2UgaGl0IG9uZSBvZiB0aHJlZSBpY29ucywgdGhleSB3b3VsZCB3aW4gb25lIG9mIHRocmVlIHByaXplcy4gR2FtZSB3YXMgYnVpbHQgdXNpbmcgR1NBUCAnICtcbiAgICAgICdhbmltYXRpb24gbGlicmFyeS4nLFxuICAgICAgaW1nOiBbXG4gICAgICAgICdpbWcvcnVud2lsZC1kZXNrdG9wMS5qcGcnLFxuICAgICAgICAnaW1nL3J1bndpbGQtZGVza3RvcDIuanBnJyxcbiAgICAgICAgJ2ltZy9ydW53aWxkLW1vYmlsZS5wbmcnLFxuICAgICAgICAnaW1nL3J1bndpbGQtbW9iaWxlLWdhbWUxLnBuZycsXG4gICAgICAgICdpbWcvcnVud2lsZC1tb2JpbGUtZ2FtZTIucG5nJyxcbiAgICAgICAgJ2ltZy9ydW53aWxkLW1vYmlsZS1nYW1lMy5wbmcnLFxuICAgICAgICAnaW1nL3J1bndpbGQtbW9iaWxlLWdhbWU0LnBuZycsXG4gICAgICAgICdpbWcvcnVud2lsZC1tb2JpbGUtZ2FtZTUucG5nJyxcbiAgICAgIF1cbiAgICB9LFxuICAgIHA2OiB7XG4gICAgICB0aXRsZTogJ0FsZmEgZ2FtZScsXG4gICAgICBkZXNjcmlwdGlvbjogJ0dhbWUgYnVpbHQgd2l0aCBQaGFzZXIgSmF2YXNjcmlwdCBsaWJyYXJ5IGZvciBhIHNoYXphbSBhY3RpdmF0aW9uLiBVc2VycycgK1xuICAgICAgJyBuZWVkIHRvIG1hdGNoIHNlbGVjdGlvbnMgdG8gd2luIGEgcHJpemUuJyxcbiAgICAgIGltZzogW1xuICAgICAgICAnaW1nL2FsZmExLW1vYmlsZTEuanBnJyxcbiAgICAgICAgJ2ltZy9hbGZhMS1tb2JpbGUyLmpwZycsXG4gICAgICAgICdpbWcvYWxmYTEtbW9iaWxlMy5qcGcnXG4gICAgICBdXG4gICAgfSxcbiAgICBwNToge1xuICAgICAgdGl0bGU6ICdMYW5kaW5nIHBhZ2VzICYgcmljaCBtZWRpYSBjcmVhdGl2ZXMnLFxuICAgICAgZGVzY3JpcHRpb246ICdMYW5kaW5nIHBhZ2VzIGFuZCByaWNoIG1lZGlhIGludGVyc3RpdGlhbHMgYnVpbHQgd2l0aCBNUkFJRCAyLjAuJyxcbiAgICAgIGltZzogW1xuICAgICAgICAnaW1nL2R2LWxwLW1vYmlsZTEuanBnJyxcbiAgICAgICAgJ2ltZy9kdi1scC1tb2JpbGUyLmpwZycsXG4gICAgICAgICdpbWcvZHYtbHAtbW9iaWxlMy5qcGcnLFxuICAgICAgICAnaW1nL2R2LXJpY2gtbW9iaWxlMS5qcGcnLFxuICAgICAgICAnaW1nL2R2LXJpY2gtbW9iaWxlMi5qcGcnLFxuICAgICAgICAnaW1nL2R2LXJpY2gtbW9iaWxlMy5qcGcnLFxuICAgICAgICAnaW1nL2R2LXJpY2gtbW9iaWxlNC5qcGcnLFxuICAgICAgICAnaW1nL2R2LXJpY2gtbW9iaWxlNS5qcGcnLFxuICAgICAgICAnaW1nL2R2LXJpY2gtbW9iaWxlNi5qcGcnLFxuICAgICAgICAnaW1nL2R2LXJpY2gtbW9iaWxlNy5qcGcnLFxuICAgICAgICAnaW1nL2R2LXJpY2gtbW9iaWxlOC5qcGcnXG4gICAgICBdXG4gICAgfSxcbiAgICBwNDoge1xuICAgICAgdGl0bGU6ICdEaWdpdGFsIFZlbnR1cmUgJiBpbi1ob3VzZSB0b29scycsXG4gICAgICBkZXNjcmlwdGlvbjogJ0RldmVsb3BtZW50IG9mIG1haW4gRGlnaXRhbCBWZW50dXJlIFdlYnNpdGUgYW5kIGluLWhvdXNlIHRvb2xzLCB1c2VkIGJ5IGJvdGggJyArXG4gICAgICAndGhlIHByb2R1Y3Rpb24gdGVhbSBhbmQgc2FsZXMgdG8gdHJhY2sgY2FtcGFpZ24gYmFzZWQgdGFza3MgYW5kIGpvdXJuZXlzLicsXG4gICAgICBpbWc6IFtcbiAgICAgICAgJ2ltZy9kdi1kZXNrdG9wMS5qcGcnLFxuICAgICAgICAnaW1nL2R2LWRlc2t0b3AyLmpwZycsXG4gICAgICAgICdpbWcvZHYtZGVza3RvcDMuanBnJyxcbiAgICAgICAgJ2ltZy9kdi1tb2JpbGUxLnBuZycsXG4gICAgICAgICdpbWcvZHYtbW9iaWxlMi5wbmcnLFxuICAgICAgICAnaW1nL2R2LW1vYmlsZTMucG5nJyxcbiAgICAgICAgJ2ltZy9kdi10b29sMS5qcGcnLFxuICAgICAgICAnaW1nL2R2LXRvb2wyLmpwZycsXG4gICAgICAgICdpbWcvZHYtdG9vbDMuanBnJyxcbiAgICAgICAgJ2ltZy9kdi10b29sNC5qcGcnLFxuICAgICAgICAnaW1nL2R2LXRvb2w1LmpwZycsXG4gICAgICAgICdpbWcvZHYtdG9vbDYuanBnJ1xuICAgICAgXVxuICAgIH0sXG4gICAgcDM6IHtcbiAgICAgIHRpdGxlOiAnR2V0IFJlYWR5IE5hcGllcicsXG4gICAgICBkZXNjcmlwdGlvbjogJ0VtcGxveWVkIGFzIGFuIGNyZWF0aXZlIGludGVybiBhdCBOYXBpZXIgbXkga2V5IHJlc3BvbnNpYmlsaXRpZXMgd2VyZSBvdmVyc2VlaW5nIHRoZSByZWRlc2lnbiBhbmQgZGV2ZWxvcG1lbnQgb2YgdGhlIG9mZmljaWFsIEdldCBSZWFkeSBzdHVkZW50IHBvcnRhbC4gRnJvbSB3b3JraW5nIGNsb3NlbHkgd2l0aCBmYWNpbGl0eSBzdGFmZiBhbmQgcGxhbm5pbmcgdGhlIGluaXRpYWwgYnJpZWYgYW5kIGRlc2lnbiB0byBmdWxsIGRldmVsb3BtZW50IG9mIHNpdGUgZmVhdHVyZXMgc3VjaCBhcyBhbmltYXRlZCBpbmZvLWdyYWhpY3MsIHVzZXIgdGVzdGluZywgcmVzcG9uc2l2ZScgK1xuICAgICAgJyBkZXNpZ24gYW5kIO+sgW5hbCBkZXBsb3ltZW50LiBcXG4nICtcbiAgICAgICdcXG4nLFxuICAgICAgaW1nOiBbXG4gICAgICAgICdpbWcvZ2V0cmVhZHktZGVza3RvcDUuanBnJyxcbiAgICAgICAgJ2ltZy9nZXRyZWFkeS1kZXNrdG9wMy5qcGcnLFxuICAgICAgICAnaW1nL2dldHJlYWR5LWRlc2t0b3AyLmpwZycsXG4gICAgICAgICdpbWcvZ2V0cmVhZHkxLW1vYmlsZS5wbmcnLFxuICAgICAgICAnaW1nL2dldHJlYWR5Mi1tb2JpbGUucG5nJ1xuICAgICAgXVxuICAgIH0sXG4gICAgcDI6IHtcbiAgICAgIHRpdGxlOiAnc2NwaWFub2xlc3NvbnMuaWUnLFxuICAgICAgZGVzY3JpcHRpb246ICdXZWJzaXRlIGRlc2lnbmVkL2RldmVsb3BlZCBmb3IgbG9jYWwgcGlhbm8gdGVhY2hlci4nLFxuICAgICAgaW1nOiBbXG4gICAgICAgICdpbWcvc2NwaWFub2xlc3NvbnMtZGVza3RvcDEuanBnJyxcbiAgICAgICAgJ2ltZy9zY3BpYW5vbGVzc29ucy1kZXNrdG9wMi5qcGcnLFxuICAgICAgICAnaW1nL3NjcGlhbm9sZXNzb25zLWRlc2t0b3AzLmpwZycsXG4gICAgICAgICdpbWcvc2NwaWFub2xlc3NvbnMtbW9iaWxlMS5wbmcnLFxuICAgICAgICAnaW1nL3NjcGlhbm9sZXNzb25zLW1vYmlsZTIucG5nJyxcbiAgICAgICAgJ2ltZy9zY3BpYW5vbGVzc29ucy1tb2JpbGUzLnBuZydcbiAgICAgIF1cbiAgICB9LFxuICAgIHAxOiB7XG4gICAgICB0aXRsZTogJ2NsYXJrZXNiYXIuaWUnLFxuICAgICAgZGVzY3JpcHRpb246ICdXZWJzaXRlIGRlc2lnbmVkL2RldmVsb3BlZCBmb3IgSXJpc2ggYmFyLicsXG4gICAgICBpbWc6IFtcbiAgICAgICAgJ2ltZy9jbGFya2VzLWRlc2t0b3AxLmpwZycsXG4gICAgICAgICdpbWcvY2xhcmtlcy1tb2JpbGUxLnBuZycsXG4gICAgICAgICdpbWcvY2xhcmtlcy1tb2JpbGUyLnBuZycsXG4gICAgICBdXG4gICAgfSxcbiAgICBzaG93UHJvamVjdDogZnVuY3Rpb24oaSkge1xuICAgICAgLy9jb25zb2xlLmxvZyh0aGlzWydwJyArIGldLnRpdGxlKVxuICAgICAgcHJvakhlYWRpbmcuaW5uZXJUZXh0ID0gdGhpc1sncCcgKyBpXS50aXRsZVxuICAgICAgcHJvakRlc2MuaW5uZXJUZXh0ID0gdGhpc1sncCcgKyBpXS5kZXNjcmlwdGlvblxuICAgICAgbGV0IHByb2plY3RJbWcgPSB0aGlzWydwJyArIGldLmltZyxcbiAgICAgICAgICBtb2JTdHIgPSAnbW9iaWxlJyxcbiAgICAgICAgICBkZXNrU3RyID0gJ2Rlc2t0b3AnXG5cbiAgICAgIGlmIChjYWNoZWQgIT09IGkpIHtcbiAgICAgICAgcHJvakltZy5pbm5lckhUTUwgPSAnJztcblxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHByb2plY3RJbWcubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICBsZXQgaW1nID0gZC5jcmVhdGVFbGVtZW50KCdpbWcnKVxuICAgICAgICAgIGltZy5zcmMgPSBwcm9qZWN0SW1nW2ldXG5cbiAgICAgICAgICBpZiAocHJvamVjdEltZ1tpXS5pbmRleE9mKG1vYlN0cikgIT09IC0xKSB7XG4gICAgICAgICAgICBpbWcuY2xhc3NOYW1lID0gJ21vYmlsZV9pbWcnXG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGltZy5jbGFzc05hbWUgPSAnZGVza3RvcF9pbWcnXG4gICAgICAgICAgfVxuICAgICAgICAgIHByb2pJbWcuYXBwZW5kQ2hpbGQoaW1nKTtcbiAgICAgICAgICBjYWNoZWQgPSBpXG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYgKCFwcm9maWxlVmlldykge1xuICAgICAgICBwcm9qZWN0VmlldyA9IHRydWVcbiAgICAgICAgVHdlZW5NYXguc2V0KFtwcm9qZWN0cywgYXJ0aWNsZXNdLCB7eTogLTIwfSlcbiAgICAgICAgdGwyXG4gICAgICAgICAgLnRvKFtwcm9qZWN0cywgYXJ0aWNsZXNdLCAwLjgsIHtcbiAgICAgICAgICAgIG9wYWNpdHk6IDAsXG4gICAgICAgICAgICB5OiAyMCxcbiAgICAgICAgICAgIG9uQ29tcGxldGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICBUd2Vlbk1heC5zZXQoW3Byb2plY3RzLCBhcnRpY2xlc10sIHtkaXNwbGF5OiAnbm9uZSd9KVxuICAgICAgICAgICAgICBUd2Vlbk1heC5zZXQoYWN0aXZlUHJvamVjdCwge3k6IC0yMH0pXG4gICAgICAgICAgICAgIFR3ZWVuTWF4LnRvKGFjdGl2ZVByb2plY3QsIDAuOCwge1xuICAgICAgICAgICAgICAgIHk6IDIwLFxuICAgICAgICAgICAgICAgIGRpc3BsYXk6ICdibG9jaycsXG4gICAgICAgICAgICAgICAgb3BhY2l0eTogMSxcbiAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9KVxuICAgICAgfVxuICAgIH0sXG4gICAgY2xvc2VQcm9qZWN0OiBmdW5jdGlvbigpIHtcbiAgICAgIHByb2plY3RWaWV3ID0gZmFsc2VcbiAgICAgIHRsMlxuICAgICAgICAudG8oYWN0aXZlUHJvamVjdCwgMC44LCB7XG4gICAgICAgICAgeTogLTI1LFxuICAgICAgICAgIG9wYWNpdHk6IDAsXG4gICAgICAgICAgZGlzcGxheTogJ25vbmUnLFxuICAgICAgICAgIG9uQ29tcGxldGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgVHdlZW5NYXgudG8oW3Byb2plY3RzLCBhcnRpY2xlc10sIDAuOCx7XG4gICAgICAgICAgICAgIHk6IC0yNSxcbiAgICAgICAgICAgICAgb3BhY2l0eTogMSxcbiAgICAgICAgICAgICAgZGlzcGxheTogJ2Jsb2NrJ1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICB9XG4gICAgICAgIH0pXG4gICAgfVxuICB9XG5cbiAgbGV0IHAgPSBPYmplY3QuY3JlYXRlKFByb2plY3RzKSxcbiAgICAgIGNsaWNrZWRQcm9qZWN0ID0gZC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdwcm9qZWN0JyksXG4gICAgICBjbG9zZVByb2plY3QgPSBkLnF1ZXJ5U2VsZWN0b3IoJy5jbG9zZV9wcm9qZWN0JylcblxuICBjbG9zZVByb2plY3QuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbihlKSB7XG4gICAgcC5jbG9zZVByb2plY3QoKVxuICB9KVxuXG4gIGZvciAobGV0IGkgPSAwOyBpIDwgY2xpY2tlZFByb2plY3QubGVuZ3RoOyBpKyspIHtcbiAgICBjbGlja2VkUHJvamVjdFtpXS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uKGUpIHtcbiAgICAgIHN3aXRjaCh0aGlzLmdldEF0dHJpYnV0ZSgnZGF0YS1pZCcpKSB7XG4gICAgICAgIGNhc2UgJzEnOlxuICAgICAgICAgIHAuc2hvd1Byb2plY3QoJzcnKVxuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlICcyJzpcbiAgICAgICAgICBwLnNob3dQcm9qZWN0KCc2JylcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSAnMyc6XG4gICAgICAgICAgcC5zaG93UHJvamVjdCgnNScpXG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgJzQnOlxuICAgICAgICAgIHAuc2hvd1Byb2plY3QoJzQnKVxuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlICc1JzpcbiAgICAgICAgICBwLnNob3dQcm9qZWN0KCczJylcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSAnNic6XG4gICAgICAgICAgcC5zaG93UHJvamVjdCgnMicpXG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgJzcnOlxuICAgICAgICAgIHAuc2hvd1Byb2plY3QoJzEnKVxuICAgICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH0pXG4gIH1cblxuICBsZXQgY2xpY2tlZEFydGljbGUgPSBkLmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ2FydGljbGUnKVxuICBmb3IgKGxldCBpID0gMDsgaSA8IGNsaWNrZWRBcnRpY2xlLmxlbmd0aDsgaSsrKSB7XG4gICAgY2xpY2tlZEFydGljbGVbaV0uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbihlKSB7XG4gICAgICBzd2l0Y2godGhpcy5nZXRBdHRyaWJ1dGUoJ2RhdGEtaWQnKSkge1xuICAgICAgICBjYXNlICdhMSc6XG4gICAgICAgICAgd2luZG93Lm9wZW4oJ2h0dHBzOi8vam9obmNhc2guaW4vc3RhZ2luZy9wb2tlZGV4L2luZGV4Lmh0bWwnLCAnX2JsYW5rJyk7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICB9XG4gICAgfSlcbiAgfVxuXG59KSgpIl19
